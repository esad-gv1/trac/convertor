
    
var activeDiv2 = null;
var compteur = 0;
var Compteur = 0;
var compteur2 = 500;

window.addEventListener("load", Setup);


function Setup() {
    
    var AudioContext = window.AudioContext // Default
    || window.webkitAudioContext // Safari and old versions of Chrome
    || false; 

if (AudioContext) {
    // Do whatever you want using the Web Audio API

    var Canvas2 = document.getElementById("Visualisateur");

    let audioIN = { audio: true }; 
    navigator.mediaDevices.getUserMedia(audioIN).then(function (mediaStreamObj) {  
        let audio = document.querySelector('audio'); 
        if ("srcObject" in audio) { 
            audio.srcObject = mediaStreamObj; 
        } 
        else {
            audio.src = window.URL.createObjectURL(mediaStreamObj); 
        }

        audio.onloadedmetadata = function (ev) { 
//                        audio.play(); 
        }; 
        let start = document.getElementById('btnStart'); 
        let stop = document.getElementById('btnStop'); 
        let info2 = document.getElementById('Info2');
        let playAudio = document.getElementById('adioPlay');
        let mediaRecorder = new MediaRecorder(mediaStreamObj);
        start.addEventListener('click', Letsgo);

        function Letsgo(ev) { 
            start.style.background = "red";
            stop.style.background="transparent";
            mediaRecorder.start(); 
            var Canvas2 = document.getElementById("Visualisateur");
            var ContexteVoix = Canvas2.getContext("2d");
            ContexteVoix.fillStyle="black";
            ContexteVoix.fillRect(0,0,500,500);

            let audioElement = document.getElementById("adioPlay");
            let audioCtx = new AudioContext();
            let analyser = audioCtx.createAnalyser();
            analyser.fftSize = 2048;
            let source = audioCtx.createMediaElementSource(audioElement);
            source.connect(analyser);
            source.connect(audioCtx.destination);
            let data = new Uint8Array(analyser.frequencyBinCount);
            requestAnimationFrame(loopingFunction);

            function loopingFunction(){
                requestAnimationFrame(loopingFunction);
                analyser.getByteFrequencyData(data);
                draw(data);
            }
            var posX=0;
            var posY=0;
            function draw(data){

                data = [...data];
                ContexteVoix.clearRect(0,0,Canvas2.width,Canvas2.height);
                let space = Canvas2.width / data.length;
                var DataLenght = data.length;
                data.forEach((value,i)=>{
                    ContexteVoix.beginPath();
                    var Weird = space*i;
                    if (value  > 100){
                        compteur++;
                        compteur2--;
                        ContexteVoix.beginPath();
                        ContexteVoix.moveTo(compteur,compteur);
                        ContexteVoix.lineTo(compteur,Canvas2.width-value);
                        ContexteVoix.strokeStyle = "black";
                        ContexteVoix.stroke();
                        ContexteVoix.closePath();
                        if (compteur > Canvas2.width){
                            compteur =0;
                        }
                        if (compteur2 < 0){
                            compteur2 = 500;
                        }

                    }
                    if (value  > 0){
                        ContexteVoix.beginPath();
                        ContexteVoix.moveTo(0,0);
                        ContexteVoix.lineTo(0,Canvas2.height-value/2);
                        ContexteVoix.lineTo(DataLenght,Canvas2.height-value/2);
                        ContexteVoix.strokeStyle = "green";
                        ContexteVoix.stroke();
                        ContexteVoix.closePath();
                    }
                    if (value > 60){
                        ContexteVoix.beginPath();
                        if (compteur > Canvas2.width){
                            compteur = 0;
                        }
                        ContexteVoix.fillRect(compteur, space*i, value, 10); //x,y
                        ContexteVoix.fillStyle = "white";
                        ContexteVoix.moveTo(compteur,compteur);
                        ContexteVoix.lineTo(space*i,Canvas2.height-value);
                        ContexteVoix.strokeStyle = "red";
                        ContexteVoix.stroke();
                        ContexteVoix.closePath();
                        if (compteur > Canvas2.width){
                            compteur =0;
                        }
                    }

                    if (DataLenght < 100){
                        ctx.strokeStyle = "blue";
                    }

                })

            }

        }
        stop.addEventListener('click', function (ev) { 
            mediaRecorder.stop(); 
            info2.style.display="block";
            playAudio.style.display="block";
            stop.style.background="green";
            start.style.background ="transparent";

        }); 
        mediaRecorder.ondataavailable = function (ev) { 
            dataArray.push(ev.data); 
        } 
        let dataArray = []; 
        mediaRecorder.onstop = function (ev) { 
            let audioData = new Blob(dataArray,{ 'type': 'audio/mp3;' }); 
            dataArray = []; 
            let audioSrc = window.URL 
            .createObjectURL(audioData); 
            playAudio.src = audioSrc; 
        } 
    }) 
        .catch(function (err) { 
        console.log(err.name, err.message); 
    });
    } else {
    // Web Audio API is not supported
    // Alert the user
    alert("Sorry, but the Web Audio API is not supported by your browser. Please, consider upgrading to the latest version or downloading Google Chrome or Mozilla Firefox");
}
}