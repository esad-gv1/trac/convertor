window.addEventListener("load", SetupMenu);
var CompteurMenu = 0;

function SetupMenu(){

//MENU NOIR BACKGROUND

var Menu = document.getElementById("Menu");
var Barre1 = document.getElementById("Barre1");
var Barre2 = document.getElementById("Barre2");
var ContainerMenu = document.getElementById("ContainerMenu");

Menu.addEventListener("mouseover", MenuIn);
Menu.addEventListener("mouseout", MenuOut);
function MenuIn(){
    Barre1.style.transform = "rotate("+25+"deg)";
    Barre2.style.transform = "rotate("+90+"deg)";
}
function MenuOut(){
    Barre1.style.transform = "rotate("+0+"deg)";
    Barre2.style.transform = "rotate("+0+"deg)";
}

Menu.addEventListener("click", MenuShow);

var Categorie1 = document.getElementById("Categorie1");
var Categorie2 = document.getElementById("Categorie2"); 
var Categorie3 = document.getElementById("Categorie3");

var P1 = document.getElementById("P1");
var P2 = document.getElementById("P2");
var P3 = document.getElementById("P3");
var P4 = document.getElementById("P4");
var P5 = document.getElementById("P5");
var P6 = document.getElementById("P6");
var P7 = document.getElementById("P7");


function MenuShow(){
    CompteurMenu++;
    
    //DEPLOIEMENT DES <P> ET COLORATION AU MOUSEENTER
    Categorie1.addEventListener("click", PagePresentation);
    Categorie1.addEventListener("mouseenter", DisplayP1P2);
    Categorie1.addEventListener("mouseout", UncolorCat1P1);
    Categorie2.addEventListener("click", GoPageLogiciels);
    Categorie2.addEventListener("mouseenter", DisplayP3P4);
    Categorie3.addEventListener("click", PageGalerie);
    Categorie3.addEventListener("mouseenter", ColorCat3);
    Categorie3.addEventListener("mouseout", UnColorCat3);

    function DisplayP1P2(){
        Categorie1.style.backgroundColor = "white";
        Categorie1.style.color = "black";
        P1.style.backgroundColor = "white";
        P1.style.color = "black";

        P1.style.display="block";
        P1.addEventListener("click", PagePresentation);
        P1.addEventListener("mouseenter", DisplayP1P2);
        P1.addEventListener("mouseout", UncolorCat1P1);

        P2.style.display="block";
        P2.addEventListener("click", GoPageEtape);
        P2.addEventListener("mouseenter", ColorP2);
        P2.addEventListener("mouseout", UnColorP2);
    }
    
    function ColorP2(){
        P2.style.backgroundColor = "white";
        P2.style.color = "black";
    }
    function UnColorP2(){
        P2.style.backgroundColor = "transparent";
        P2.style.color = "white";
    }
    function UncolorCat1P1(){
        Categorie1.style.backgroundColor = "transparent";
        Categorie1.style.color = "white";
        P1.style.backgroundColor = "transparent";
        P1.style.color = "white";
    }

    function DisplayP3P4(){
        Categorie2.style.backgroundColor = "white";
        Categorie2.style.color = "black";
        P3.style.backgroundColor = "white";
        P3.style.color = "black";
        P3.style.display="block";
        P3.addEventListener("click", GoPageLogiciels);
        Categorie2.addEventListener("mouseenter", ColorCat2P3);
        Categorie2.addEventListener("mouseout", UncolorCat2P3);
        P3.addEventListener("mouseenter", ColorCat2P3);
        P3.addEventListener("mouseout", UncolorCat2P3);

        P4.style.display="block";
        P4.addEventListener("click", GoPageTelecharger);
        P4.addEventListener("mouseenter", ColorP4);
        P4.addEventListener("mouseout", UnColorP4);
    }
    function ColorCat2P3(){
        Categorie2.style.backgroundColor = "white";
        Categorie2.style.color = "black";
        P3.style.backgroundColor = "white";
        P3.style.color = "black";
    }
    function UncolorCat2P3(){
        Categorie2.style.backgroundColor = "transparent";
        Categorie2.style.color = "white";
        P3.style.backgroundColor = "transparent";
        P3.style.color = "white";
    }
    function ColorP4(){
        P4.style.backgroundColor = "white";
        P4.style.color = "black";
    }
    function UnColorP4(){
        P4.style.backgroundColor = "transparent";
        P4.style.color = "white"; 
    }

    function ColorCat3(){
        Categorie3.style.backgroundColor = "white";
        Categorie3.style.color="black";
    }
    function UnColorCat3(){
        Categorie3.style.backgroundColor = "transparent";
        Categorie3.style.color="white";
    }
    //REDIRECTION DES CATEGORIES VERS LES PAGES
    
    function PagePresentation(){
        window.location ="1_01_Le_projet.html";
    }
    function GoPageEtape(){
        window.location ="https://esad-gv.net/designgraphique/DG5/Samantha_Zannoni/convertor.git/1_02_Etapes.html";
    }
    function GoPageLogiciels(){
        window.location = "2_01_Logiciels.html";
    }
    function GoPageTelecharger(){
        window.location = "https://github.com/martinbellec/SonEtImage";
    }
    function PageGalerie(){
         window.location ="3_Experimentations.html";
    }
    
    if (CompteurMenu === 1){
        ContainerMenu.style.width = 20 + "vw";
        Barre1.style.backgroundColor ="white";
        Barre2.style.backgroundColor ="white";
    } else {
        ContainerMenu.style.width = 0 +"vw";
        Barre1.style.backgroundColor ="#0b0b0b";
        Barre2.style.backgroundColor ="#0b0b0b"; 
        CompteurMenu = 0;
    }

}



}